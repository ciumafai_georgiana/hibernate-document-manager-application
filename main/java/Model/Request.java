package Model;

import javax.persistence.*;

@Entity
@Table(name = "request")
public class Request {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "IdRequest", nullable = false)
    private int IdReq;

    @Column(name = "Status",length = 20, nullable = false)
    private String stat;

    @ManyToOne
    @JoinColumn(name="Email",referencedColumnName = "Email")
    private User email;

    @ManyToOne
    @JoinColumn(name="IdHouse",referencedColumnName = "IdHouse")
    private House IdHouse;

    @ManyToOne
    @JoinColumn(name="IdDocument",referencedColumnName = "IdDocument")
    private Document IdDocument;

    public Request(String stat, User email, House idHouse, Document idDocument) {
        this.stat = stat;
        this.email = email;
        IdHouse = idHouse;
        IdDocument = idDocument;
    }

    public Request(String stat, User email) {
        this.stat = stat;
        this.email = email;
    }

    public Request(){

    }

    @Override
    public String toString() {
        return "Request{" +
                "IdReq=" + IdReq +
                ", stat='" + stat + '\'' +
                ", email=" + email.getEmail() +
                ", IdHouse=" + IdHouse.getId_house() +
                ", IdDocument=" + IdDocument.getIdDocument() +
                '}';
    }

    public Request(int idReq, String stat, User email, House idHouse, Document idDocument) {
        IdReq = idReq;
        this.stat = stat;
        this.email = email;
        IdHouse = idHouse;
        IdDocument = idDocument;
    }

    public int getIdReq() {
        return IdReq;
    }

    public Request(String stat, User email, Document idDocument) {
        this.stat = stat;
        this.email = email;
        IdDocument = idDocument;
    }

    public void setIdReq(int idReq) {
        IdReq = idReq;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public User getEmail() {
        return email;
    }

    public void setEmail(User email) {
        this.email = email;
    }

    public House getIdHouse() {
        return IdHouse;
    }

    public void setIdHouse(House idHouse) {
        IdHouse = idHouse;
    }

    public Document getIdDocument() {
        return IdDocument;
    }

    public void setIdDocument(Document idDocument) {
        IdDocument = idDocument;
    }
}
