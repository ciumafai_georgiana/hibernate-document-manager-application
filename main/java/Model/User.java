package Model;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name = "Email",length = 20, nullable = false)
    private String email;

    @Column(name = "Password",length = 20, nullable = false)
    private String password;

    @Column(name = "Type",length = 10, nullable = false)
   private String type;

    public User(String email, String password, String name) {
        this.email = email;
        this.password = password;
        this.name = name;
    }

    @Column(name = "Name",length = 30, nullable = false)
    private String name;

    public User() {
    }

    public User(String email, String password, String type,String name) {
        this.email = email;
        this.password = password;
        this.type = type;
        this.name=name;

    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "User: " + this.email + ", " + this.name+ ", " + this.type;
    }

}