package Model;

import javax.persistence.*;


@Entity
@Table(name = "document")
public class Document {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "IdDocument")
    private int IdDocument;

    @Column(name = "DocumentType",length = 25, nullable = false)
    private String doc_type;

    public Document(String doc_type) {
        this.doc_type = doc_type;
    }
    public Document(){

    }
    @Override
    public String toString() {
        return "Document{" +
                "IdDocument=" + IdDocument +
                ", doc_type='" + doc_type + '\'' +
                '}';
    }

    public Document(int idDocument, String doc_type) {
        IdDocument = idDocument;
        this.doc_type = doc_type;
    }

    public int getIdDocument() {
        return IdDocument;
    }

    public void setIdDocument(int idDocument) {
        IdDocument = idDocument;
    }

    public String getDoc_type() {
        return doc_type;
    }

    public void setDoc_type(String doc_type) {
        this.doc_type = doc_type;
    }
}
