package Model;

import javax.persistence.*;

@Entity
@Table(name = "house")
public class House {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "IdHouse")
    private int IdHouse;

    @ManyToOne
    @JoinColumn(name="Email",referencedColumnName = "Email")
    private User email;

    @Column(name = "Address",length = 20, nullable = false)
    private String address;


    public House(User email, String address) {
        this.email = email;
        this.address = address;
    }

    public User getEmail() {
        return email;
    }

    public void setEmail(User email) {
        this.email = email;
    }

    public int getId_house() {
        return IdHouse;
    }

    public void setId_house(int IdHouse) {
        this.IdHouse = IdHouse;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public House() {
    }

    public House(int id_house, User email, String address) {
        this.IdHouse = id_house;
        this.email = email;
        this.address = address;
    }

    @Override
    public String toString() {
        return "House{" +
                "id_house=" + IdHouse +
                ", email='" + email.getEmail() + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
