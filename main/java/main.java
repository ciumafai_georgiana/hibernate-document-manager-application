import BLL.DocumentBLL;
import BLL.HouseBLL;
import BLL.RequestBLL;
import BLL.UserBLL;
import Model.Document;
import Model.House;
import Model.Request;
import Model.User;

import java.util.List;
import java.util.Scanner;
import java.util.logging.LogManager;

public class main {
    public static void disableWarning() {
        System.err.close();
        System.setErr(System.out);
    }

    public static void main(String[] args) {

        LogManager.getLogManager().reset();
        disableWarning();

        UserBLL userBLL = new UserBLL();
        HouseBLL houseBLL = new HouseBLL();
        DocumentBLL documentBLL = new DocumentBLL();
        RequestBLL requestBLL = new RequestBLL();
        boolean stop = false;
        System.out.println("1 - Register");
        System.out.println("2 - Login");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        while (stop != true) {
            switch (choice) {
                case 1:
                    System.out.println("Name: ");
                    Scanner scanner1 = new Scanner(System.in);
                    String name = scanner1.next();
                    System.out.println("\n Email: ");
                    String email = scanner1.next();
                    System.out.println("\n Password: ");
                    String password = scanner1.next();
                    User unou = userBLL.register(email, password, name);
                    break;
                case 2:
                    Scanner scanner2 = new Scanner(System.in);
                    System.out.println("\n Email: ");
                    String email1 = scanner2.next();
                    System.out.println("\n Password: ");
                    String password1 = scanner2.next();
                    if (userBLL.login2(email1, password1) == 1) {
                        if(userBLL.findById(email1).getType().equals("cetatean")) {
                            System.out.println("Logare cu succes");
                            System.out.println("3-  Add house");
                            System.out.println("4-  Delete house");
                            System.out.println("5-  View Requests");
                            System.out.println("6-  Add request");
                            System.out.println("7- Update Request");
                            System.out.println("8-Delete Request");
                            int choice2 = scanner.nextInt();
                            switch (choice2) {
                                case 3:
                                    System.out.println("Adress:");
                                    Scanner s3 = new Scanner(System.in);
                                    String address;
                                    if (s3.hasNextLine()) {
                                        address = s3.nextLine();
                                    } else {
                                        address = null;
                                    }
                                    House hn = new House(userBLL.findById(email1), address);
                                    houseBLL.persist(hn);
                                    break;
                                case 4:
                                    System.out.println("your houses are:");
                                    List<House> houses2 = houseBLL.findAllbyuser(userBLL.findById(email1));
                                    for (House ho : houses2) {
                                        System.out.println(ho.toString());
                                    }
                                    if (houses2.isEmpty()) {
                                        System.out.println("empty");
                                    } else {
                                        System.out.println("Choose ID House to delete: ");
                                        String idLoc = scanner.next();
                                        int id = Integer.parseInt(idLoc);
                                        houseBLL.delete(houseBLL.findById(id));
                                    }
                                    break;
                                case 5:
                                    List<Request> requests = requestBLL.findAll();
                                    System.out.println("Requests are :");
                                    for (Request r : requests) {
                                        if (r.getEmail().getEmail().equals(email1)) {
                                            System.out.println("-" + r.toString());
                                        }
                                    }
                                    break;
                                case 6:
                                    System.out.println("impozit");
                                    System.out.println("adeverinta");
                                    System.out.println("instiintare");
                                    System.out.println("Introduceti tipul dorit:");
                                    String tipDoc = scanner.next();
                                    System.out.println("your houses are:");
                                    List<House> houses = houseBLL.findAllbyuser(userBLL.findById(email1));
                                    for (House h : houses)
                                        System.out.println("-" + h.toString());
                                    System.out.println("Choose the house id:");
                                    int id5 = scanner.nextInt();
                                    Document doc = new Document(documentBLL.findByType(tipDoc), tipDoc);
                                    List<Request> lreq=requestBLL.findAllByUserwithType(userBLL.findById(email1),doc);
                                    if(lreq.size()<3) {
                                        requestBLL.addRequest(userBLL.findById(email1), houseBLL.findById(id5), doc);
                                        System.out.println("Request add succesfully");
                                    }
                                    else
                                    {
                                        System.out.println("Cannot add more requests for this user");
                                    }
                                    break;
                                case 7:
                                    System.out.println("your requests are:");
                                    List<Request> requesturi1 = requestBLL.findAllByUser(userBLL.findById(email1));
                                    for (Request re2 : requesturi1)
                                        System.out.println("-" + re2.toString());
                                    if (requesturi1.isEmpty()) {
                                        System.out.println("empty");
                                    }
                                    if (requesturi1.isEmpty() == false) {
                                        System.out.println("ID Request: ");
                                        String idRe = scanner.next();
                                        int id2 = Integer.parseInt(idRe);
                                        Request r = requestBLL.findById(id2);
                                        System.out.println("Change document Type: ");
                                        String tipDoc2 = scanner.next();
                                        System.out.println("your houses are:");
                                        List<House> houses3 = houseBLL.findAllbyuser(userBLL.findById(email1));
                                        for (House h : houses3)
                                            System.out.println("-" + h.toString());
                                        System.out.println("Choose the house id:");
                                        int id6 = scanner.nextInt();
                                        Document d2 = new Document(documentBLL.findByType(tipDoc2), tipDoc2);
                                        r.setIdDocument(d2);
                                        r.setIdHouse(houseBLL.findById(id6));
                                        requestBLL.update(r);
                                    }
                                    break;
                                case 8:

                                    System.out.println("your requests are:");
                                    List<Request> requesturi = requestBLL.findAllByUser(userBLL.findById(email1));
                                    for (Request re : requesturi)
                                        System.out.println("-" + re.toString());
                                    if (requesturi.isEmpty()) {
                                        System.out.println("empty");
                                    }
                                    if (requesturi.isEmpty() == false) {
                                        System.out.println("Choose the Request id:");
                                        String idReq = scanner.next();
                                        int id3 = Integer.parseInt(idReq);
                                        requestBLL.delete(requestBLL.findById(id3));
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            System.out.println("Admin Logat cu succes");
                            System.out.println("1-  View all users");
                            System.out.println("2-  Add doc type");
                            System.out.println("3-  Delete doc type");
                            System.out.println("4-  View all requests");
                            System.out.println("5- Approve Request");
                            System.out.println("6-Delete Request");
                            int choice3 = scanner.nextInt();
                            switch (choice3) {
                                case 1:
                                    System.out.println(" users are:");
                                    List<User> users2 = userBLL.findAll();
                                    for (User usr2 : users2)
                                        System.out.println("-" + usr2.toString());
                                    if (users2.isEmpty()) {
                                        System.out.println("empty");
                                    }
                                    break;
                                case 2:
                                    System.out.println("Choose document type to add:");
                                    String tipDoc3 = scanner.next();
                                    Document d3=new Document(tipDoc3);
                                    documentBLL.persist(d3);
                                    break;
                                case 3:
                                    System.out.println("Choose document type to delete:");
                                    String tipDoc4 = scanner.next();
                                    documentBLL.delete(documentBLL.findById(documentBLL.findByType(tipDoc4)));
                                    break;
                                case 4:
                                    System.out.println(" requests are:");
                                    List<Request> requesturin = requestBLL.findAll();
                                    for (Request ren : requesturin)
                                        System.out.println("-" + ren.toString());
                                    if (requesturin.isEmpty()) {
                                        System.out.println("empty");
                                    }
                                    break;
                                case 5:
                                    System.out.println(" request id to approve:");
                                    int id7=scanner.nextInt();
                                    Request r5=requestBLL.findById(id7);
                                    r5.setStat("aprobat");
                                    requestBLL.update(r5);
                                    break;
                                case 6:
                                    System.out.println("request id to delete");
                                    int id8=scanner.nextInt();
                                    requestBLL.delete(requestBLL.findById(id8));
                                    break;
                            }
                        }
                    } else
                        System.out.println("Ai gresit parola");
                    break;
                    default:
            }
        }
    }

}
