package Dao;

import java.util.List;
import Model.House;
import Model.User;
import org.hibernate.Query;


public class HouseDao {
    private SessionUtil session=new SessionUtil();


    public void persist(House entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().save(entity);
        session.closeCurrentSessionwithTransaction();
    }

    public void update(House entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().update(entity);
        session.closeCurrentSessionwithTransaction();
    }

    public House findById(int id) {
        session.openCurrentSessionwithTransaction();
        House house = (House) session.getCurrentSession().get(House.class, id);
        session.closeCurrentSessionwithTransaction();
        return house;
    }

    public void delete(House entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().delete(entity);
        session.closeCurrentSessionwithTransaction();
    }

    @SuppressWarnings("unchecked")
    public List<House> findAll() {
        session.openCurrentSessionwithTransaction();
        List<House> houses = (List<House>) session.getCurrentSession().createQuery("from House").list();
        session.closeCurrentSessionwithTransaction();
        return houses;
    }

    public void deleteAll() {

        List<House> entityList = findAll();
        for (House entity : entityList) {
            delete(entity);
        }
    }
    public List<House> findAllbyuser(User u) {
        session.openCurrentSessionwithTransaction();
        Query query=session.getCurrentSession().createQuery("from House where email = :email");
        query.setParameter("email",u);
        List<House> houses = (List<House>) query.list();
        session.closeCurrentSessionwithTransaction();
        return houses;
    }

}
