package Dao;

import java.util.List;

import Model.Document;
import Model.House;
import Model.Request;
import Model.User;
import org.hibernate.Query;


public class RequestDao {
    private SessionUtil session=new SessionUtil();


    public void persist(Request entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().save(entity);
        session.closeCurrentSessionwithTransaction();
    }

    public void update(Request entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().update(entity);
        session.closeCurrentSessionwithTransaction();
    }

    public Request findById(int id) {
        session.openCurrentSessionwithTransaction();
        Request request = (Request) session.getCurrentSession().get(Request.class, id);
        session.closeCurrentSessionwithTransaction();
        return request;
    }

    public void delete(Request entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().delete(entity);
        session.closeCurrentSessionwithTransaction();
    }

    @SuppressWarnings("unchecked")
    public List<Request> findAll() {
        session.openCurrentSessionwithTransaction();
        List<Request> requests = (List<Request>) session.getCurrentSession().createQuery("from Request").list();
        session.closeCurrentSessionwithTransaction();
        return requests;
    }

    public void deleteAll() {

        List<Request> entityList = findAll();
        for (Request entity : entityList) {
            delete(entity);
        }
    }
    public void addRequest(User u, House h, Document doc){
        Request r=new Request("neaprobat",u,h,doc);
        persist(r);
    }
    public List<Request> findAllByUser(User u) {
        session.openCurrentSessionwithTransaction();
        Query query=session.getCurrentSession().createQuery("from Request where email = :email");
        query.setParameter("email",u);
        List<Request> requests = (List<Request>) query.list();
        session.closeCurrentSessionwithTransaction();
        return requests;
    }
    public List<Request> findAllByUserwithType(User u,Document d) {
        session.openCurrentSessionwithTransaction();
        Query query=session.getCurrentSession().createQuery("from Request where email = :email and IdDocument = :IdDocument");
        query.setParameter("email",u);
        query.setParameter("IdDocument",d);
        List<Request> requests2 = (List<Request>) query.list();
        session.closeCurrentSessionwithTransaction();
        return requests2;
    }
}
