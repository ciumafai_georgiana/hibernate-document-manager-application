package Dao;

import java.util.List;
import Model.User;


public class UserDao {

    private SessionUtil session=new SessionUtil();


    public void persist(User entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().save(entity);
        session.closeCurrentSessionwithTransaction();
    }

    public void update(User entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().update(entity);
        session.closeCurrentSessionwithTransaction();
    }

    public User findById(String id) {
        session.openCurrentSessionwithTransaction();
        User user = (User) session.getCurrentSession().get(User.class, id);
        session.closeCurrentSessionwithTransaction();
        return user;
    }

    public void delete(User entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().delete(entity);
        session.closeCurrentSessionwithTransaction();
    }

    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        session.openCurrentSessionwithTransaction();
        List<User> user = (List<User>) session.getCurrentSession().createQuery("from User").list();
        session.closeCurrentSessionwithTransaction();
        return user;
    }

    public void deleteAll() {
        List<User> entityList = findAll();
        for (User entity : entityList) {
            delete(entity);
        }
    }


}
 
 