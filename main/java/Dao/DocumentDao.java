package Dao;


import java.util.List;
import Model.Document;
import org.hibernate.Query;

public class DocumentDao {
    private SessionUtil session=new SessionUtil();


    public void persist(Document entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().save(entity);
        session.closeCurrentSessionwithTransaction();
    }

    public void update(Document entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().update(entity);
        session.closeCurrentSessionwithTransaction();
    }

    public Document findById(int id) {
        session.openCurrentSessionwithTransaction();
        Document doc = (Document) session.getCurrentSession().get(Document.class, id);
        session.closeCurrentSessionwithTransaction();
        return doc;
    }

    public void delete(Document entity) {
        session.openCurrentSessionwithTransaction();
        session.getCurrentSession().delete(entity);
        session.closeCurrentSessionwithTransaction();
    }

    @SuppressWarnings("unchecked")
    public List<Document> findAll() {
        session.openCurrentSessionwithTransaction();
        List<Document> documents = (List<Document>) session.getCurrentSession().createQuery("from Document").list();
        session.closeCurrentSessionwithTransaction();
        return documents;
    }

    public void deleteAll() {

        List<Document> entityList = findAll();
        for (Document entity : entityList) {
            delete(entity);
        }
    }
    public int findByType(String s) {
        session.openCurrentSessionwithTransaction();
        Query query=session.getCurrentSession().createQuery("from Document where doc_type = :doc_type");
        query.setParameter("doc_type",s);
        List<Document> q=query.list();
        session.closeCurrentSessionwithTransaction();
        return q.get(0).getIdDocument();
    }
}
