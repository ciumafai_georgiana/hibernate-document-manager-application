package BLL;

import Dao.HouseDao;
import Model.House;
import Model.User;

import java.util.List;

public class HouseBLL {
    private static HouseDao houseDao;


    public HouseBLL() {

        houseDao = new HouseDao();
    }
    public void persist(House entity) {

        houseDao.persist(entity);
    }
    @SuppressWarnings("unchecked")
    public List<House> findAll() {
        List<House> houses=houseDao.findAll();
        return houses;
    }
    public void deleteAll() {

        houseDao.deleteAll();

    }
    public void update(House entity) {

        houseDao.update(entity);
    }

    public House findById(int id) {
        House house = houseDao.findById(id);
        return house;
    }

    public void delete(House entity) {
        houseDao.delete(entity);
    }

    public HouseDao houseDao() {
        return houseDao;
    }

    public List<House> findAllbyuser(User u) {
        List<House> houses=houseDao.findAllbyuser(u);
        return houses;
    }
    public House findHouseByUser(User u,int id){
        List<House> houses=houseDao.findAllbyuser(u);
        for(House h:houses){
            if(id==h.getId_house())
                return h;
        }
        return null;
    }
}
