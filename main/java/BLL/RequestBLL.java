package BLL;

import Dao.RequestDao;
import Model.Document;
import Model.House;
import Model.Request;
import Model.User;

import java.util.List;

public class RequestBLL {
    private static RequestDao requestDao;
    public RequestBLL() {

        requestDao = new RequestDao();
    }
    public void persist(Request entity) {

        requestDao.persist(entity);
    }
    @SuppressWarnings("unchecked")
    public List<Request> findAll() {
        List<Request> requests=requestDao.findAll();
        return requests;
    }
    public void deleteAll() {

        requestDao.deleteAll();

    }
    public void update(Request entity) {

        requestDao.update(entity);
    }

    public Request findById(int id) {
        Request request = requestDao.findById(id);
        return request;
    }

    public void delete(Request entity) {

        requestDao.delete(entity);
    }

    public RequestDao userDao() {
        return requestDao;
    }

    public void addRequest(User u, House h, Document doc){

        requestDao.addRequest(u,h,doc);
    }
    public List<Request> findAllByUser(User u) {
        List<Request> requests=requestDao.findAllByUser(u);
        return requests;
    }
    public List<Request> findAllByUserwithType(User u,Document d) {
        List<Request> requests2 = requestDao.findAllByUserwithType(u,d);
        return requests2;
    }


}
