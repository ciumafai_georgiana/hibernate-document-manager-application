package BLL;

import Dao.DocumentDao;
import Model.Document;

import javax.print.Doc;
import java.util.List;

public class DocumentBLL {
    private static DocumentDao documentDao;
    public DocumentBLL() {

        documentDao = new DocumentDao();
    }
    public void persist(Document entity) {

        documentDao.persist(entity);
    }
    @SuppressWarnings("unchecked")
    public List<Document> findAll() {
        List<Document> documents=documentDao.findAll();
        return documents;
    }
    public void deleteAll() {

        documentDao.deleteAll();

    }
    public void update(Document entity) {

        documentDao.update(entity);
    }

    public Document findById(int id) {
        Document document = documentDao.findById(id);
        return document;
    }

    public void delete(Document entity) {
        documentDao.delete(entity);
    }
    public DocumentDao documentDao() {
        return documentDao;
    }
    public int findByType(String s) {
       return documentDao().findByType(s);
    }

}
