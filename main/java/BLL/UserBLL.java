package BLL;


import Dao.UserDao;

import Model.User;

import java.util.List;


public class UserBLL {

    private static UserDao userDao;
    public UserBLL() {

        userDao = new UserDao();
    }
    public void persist(User entity) {

        userDao.persist(entity);
    }
    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        List<User> users=userDao.findAll();
        return users;
    }
    public void deleteAll() {

        userDao.deleteAll();

    }
    public void update(User entity) {

        userDao.update(entity);
    }

    public User findById(String id) {
        User user = userDao.findById(id);
        return user;
    }

    public void delete(User entity) {

        userDao.delete(entity);
    }

    public UserDao userDao() {
        return userDao;
    }

    public User register(String email,String password,String name){
        User u=new User();
        u.setEmail(email);
        u.setPassword(password);
        u.setName(name);
        u.setType("cetatean");
        userDao.persist(u);
        return u;
    }

    public int login2(String email,String password){
        List<User> users=findAll();
        //User user=new User();
        int ok=0;
        try {
            for(User u:users){
                if (u.getPassword().equals(password)&& u.getEmail().equals(email)) {
                    System.out.println("Login-ul a avut succes");
                    ok=1;
                }
            }
        }
        catch(Exception e)
        {
            System.out.println("Eroare de logare");
            ok=0;
        }
        return ok;
    }


}
