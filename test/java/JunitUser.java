import BLL.DocumentBLL;
import BLL.HouseBLL;
import BLL.RequestBLL;
import BLL.UserBLL;
import Model.Document;
import Model.House;
import Model.Request;
import Model.User;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.List;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JunitUser {

    UserBLL userBLL=new UserBLL();
    HouseBLL houseBLL=new HouseBLL();
    RequestBLL requestBLL=new RequestBLL();
    DocumentBLL documentBLL=new DocumentBLL();
    //merge
    @Test
	public void testInserUser() {
		User usern=userBLL.register("email6","pass","Alin");
		System.out.println(usern);
		Assert.assertEquals("cetatean", userBLL.findById("email6").getType());
		userBLL.delete(usern);
    }

	//merge
	@Test
	public void testLogare(){
		int i=userBLL.login2("email", "pass");
		Assert.assertEquals(1,i);
	}

	//merge
	@Test
	public void testAddHouse(){
	    User u1=new User("email7","pass","cetatean","Ana Maria");
	    House h1=new House(u1,"Bucuresti 29");
        userBLL.persist(u1);
	    houseBLL.persist(h1);
	    int id=h1.getId_house();
        List<House> houses2=houseBLL.findAllbyuser(u1);
        Assert.assertEquals(houses2.get(0).getId_house(),id);
        userBLL.delete(u1);
        //houseBLL.delete(h1);
    }
/*
    @Test
    public void testDeleteHouse(){
        User u3=new User("email5","pass","cetatean","Ana");
        House h3=new House(u3,"Gorunului 19");
        userBLL.persist(u3);
        houseBLL.persist(h3);
        houseBLL.delete(h3);
        Assert.assertEquals(null,houseBLL.findHouseByUser(u3,h3.getId_house()));
    }
    */
    @Test
    public void testAddRequest(){
        User u=new User("email8","pass","cetatean","ana");
        House h=new House(u,"Plopilor 28");
        Document d=new Document("tip nou doc");
        userBLL.persist(u);
        houseBLL.persist(h);
        documentBLL.persist(d);
        requestBLL.addRequest(u,h,d);
        List<Request> relist=requestBLL.findAllByUser(u);
        if(relist.size()>0){
            Assert.assertEquals(relist.get(0).getEmail().getEmail(),u.getEmail());
            Assert.assertEquals(relist.get(0).getIdDocument().getIdDocument(),d.getIdDocument());
            Assert.assertEquals(relist.get(0).getIdHouse().getId_house(),h.getId_house());
            userBLL.delete(u);
            houseBLL.delete(h);
            documentBLL.delete(d);
        }
    }
}
